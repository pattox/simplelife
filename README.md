## SimpleLife

This mod contains several items, recipes and features to make life
a little easier. 

**Items**
- Bleach
    * remove color from beds, banners, wool, carpets
- Salt
    * Used for creating bleach

- Lamps
    * Eight lamps in different colors with aa very basic texture
    and a luminance of 10, so that the mobs stay away
      

**Recipes**

- Black Dye from Charcoal
    * A bowl, a bone and some charcoal make black dye. Animal-friendly.
    
- Brown Dye from mixing
    * Add red, yellow and blue dye together in a crafting bench to 
    get brown dye (instead of searching for cocoa for a thousand years)
    
- Bleach
    * You need Salt, a bottle, and some redstone to get the bleach.
    
- Salt
    * Put a bottle of water in a furnace, burn the water away and keep 
    the salt!

- Lamps
    * The proper colored dye surrounded by redstone gives you some lamps
    
- String to Wool
    * The almost famous addition to the pack. 4 strings make 1 wool.
    
- White banners from bleaching
    * Add any colored banner with some bleach to a crafting table to get
    the white version.
      
- White beds from bleaching
    * Add any colored bed with some bleach to a crafting table to get 
      the white version
      
- White carpets from bleaching
    * Add any colored... yeah, you get it.
    
- White wool from bleaching
    * .... needs explanation?
    

    
    
