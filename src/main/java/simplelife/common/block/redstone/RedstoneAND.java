package simplelife.common.block.redstone;

import net.minecraft.block.Block;
import net.minecraft.block.BlockRenderType;
import net.minecraft.block.BlockState;
import net.minecraft.block.BlockWithEntity;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.item.ItemPlacementContext;
import net.minecraft.state.StateManager;
import net.minecraft.state.property.Properties;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.world.BlockView;
import net.minecraft.world.World;
import net.minecraft.world.WorldAccess;
import simplelife.common.SimpleLife;
import simplelife.common.block.entities.RedstoneANDEntity;

@SuppressWarnings("deprecation")
public class RedstoneAND extends BlockWithEntity implements Redstoneable {

    public static Identifier identifier = new Identifier(SimpleLife.MOD_ID, "redstone_and");

    public RedstoneAND(Settings settings) {
        super(settings);

        // On initial state, the redstone-lock doesn't provide power.
        setDefaultState(this.stateManager.getDefaultState()
                .with(Properties.POWER, 0)
                .with(Properties.HORIZONTAL_FACING, Direction.NORTH)
                .with(Properties.POWERED, false)
        );
    }

    @Override
    public BlockState getStateForNeighborUpdate(BlockState state, Direction direction, BlockState newState, WorldAccess world, BlockPos pos, BlockPos posFrom) {
        Direction eastDirection = state.get(Properties.HORIZONTAL_FACING).rotateYClockwise();
        Direction westDirection = state.get(Properties.HORIZONTAL_FACING).rotateYCounterclockwise();

        World w = (World) world;
        boolean east = w.isEmittingRedstonePower(pos.offset(eastDirection, 1), eastDirection);
        boolean west = w.isEmittingRedstonePower(pos.offset(westDirection, 1), westDirection);

        if (east && west) {
            state = state.with(Properties.POWERED, true);
            state = state.with(Properties.POWER, 15);
        } else {
            state = state.with(Properties.POWER, 0);
            state = state.with(Properties.POWERED, false);
        }
        return state;
    }

    @Override
    public BlockEntity createBlockEntity(BlockView world) {
        return new RedstoneANDEntity();
    }

    @Override
    protected void appendProperties(StateManager.Builder<Block, BlockState> stateManager) {
        stateManager.add(
                Properties.POWER,
                Properties.POWERED,
                Properties.HORIZONTAL_FACING)
        ;
    }

    @Override
    public BlockRenderType getRenderType(BlockState state) {
        return BlockRenderType.MODEL;
    }

    @Override
    public boolean emitsRedstonePower(BlockState state) {
        return false;
    }

    @Override
    public boolean hasComparatorOutput(BlockState state) {
        return true;
    }

    @Override
    public BlockState getPlacementState(ItemPlacementContext ctx) {
        return this.getDefaultState().with(Properties.HORIZONTAL_FACING, ctx.getPlayerFacing());
    }

    // Returns the signal for this block. So 'true' when the proper key is inserted
    @Override
    public int getComparatorOutput(BlockState state, World world, BlockPos pos) {
        return state.get(Properties.POWER);
    }

    @Override
    public int getWeakRedstonePower(BlockState state, BlockView world, BlockPos pos, Direction direction) {
        // Rotating, so we only output at the back
        if (direction.getOpposite() == state.get(Properties.HORIZONTAL_FACING)) {
            return state.get(Properties.POWER);
        } else {
            return 0;
        }
    }

    @Override
    public int getStrongRedstonePower(BlockState state, BlockView world, BlockPos pos, Direction direction) {
        return this.getWeakRedstonePower(state, world, pos, direction);

    }

}
