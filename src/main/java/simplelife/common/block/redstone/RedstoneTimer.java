package simplelife.common.block.redstone;

import net.minecraft.block.Block;
import net.minecraft.block.BlockRenderType;
import net.minecraft.block.BlockState;
import net.minecraft.block.BlockWithEntity;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.item.ItemPlacementContext;
import net.minecraft.state.StateManager;
import net.minecraft.state.property.Properties;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.world.BlockView;
import net.minecraft.world.World;
import simplelife.common.block.entities.RedstoneTimerEntity;

@SuppressWarnings("deprecation")
public class RedstoneTimer extends BlockWithEntity implements Redstoneable {

    public RedstoneTimer(Settings settings) {
        super(settings);

        // On initial state, the redstone-lock doesn't provide power.
        setDefaultState(this.stateManager.getDefaultState()
                .with(Properties.POWER, 0)
                .with(Properties.POWERED, false)
        );
    }

    @Override
    public BlockEntity createBlockEntity(BlockView world) {
        return new RedstoneTimerEntity();
    }

    @Override
    protected void appendProperties(StateManager.Builder<Block, BlockState> stateManager) {
        stateManager.add(
                Properties.POWER,
                Properties.HORIZONTAL_FACING,
                Properties.POWERED);
    }

    @Override
    public BlockRenderType getRenderType(BlockState state) {
        return BlockRenderType.MODEL;
    }

    @Override
    public boolean emitsRedstonePower(BlockState state) {
        return false;
    }

    @Override
    public boolean hasComparatorOutput(BlockState state) {
        return true;
    }

    @Override
    public BlockState getPlacementState(ItemPlacementContext ctx) {
        return this.getDefaultState().with(Properties.HORIZONTAL_FACING, ctx.getPlayerFacing());
    }

    // Returns the signal for this block. So 'true' when the proper key is inserted
    @Override
    public int getComparatorOutput(BlockState state, World world, BlockPos pos) {
        return state.get(Properties.POWER);
    }

    @Override
    public int getWeakRedstonePower(BlockState state, BlockView world, BlockPos pos, Direction direction) {
        // Rotating, so we only output at the back
        if (direction.getOpposite() == state.get(Properties.HORIZONTAL_FACING)) {
            return state.get(Properties.POWER);
        } else {
            return 0;
        }
    }

    @Override
    public int getStrongRedstonePower(BlockState state, BlockView world, BlockPos pos, Direction direction) {
        return this.getWeakRedstonePower(state, world, pos, direction);

    }
}
