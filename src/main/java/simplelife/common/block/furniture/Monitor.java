package simplelife.common.block.furniture;

@SuppressWarnings("deprecation")
public class Monitor extends BasicDirectionalFurniture {

    public static String identifier = "monitor";

    public Monitor(Settings settings) {
        super(settings);
        this.setVoxelShape(0, 0, (6F / 16F), 1, 1, (9F / 16F));
    }

}
