package simplelife.common.block.furniture;

public class PartBackPlate extends BasicDirectionalFurniture {

    public static String identifier = "part_back_plate";

    public PartBackPlate(Settings settings) {
        super(settings);
        this.setVoxelShape(0, 0, 0, 1F, 1F, (1F/16F));
    }
}
