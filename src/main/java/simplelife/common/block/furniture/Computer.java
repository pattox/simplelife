package simplelife.common.block.furniture;

public class Computer extends BasicDirectionalFurniture {

    public static String identifier = "computer";

    public Computer(Settings settings) {
        super(settings);
        this.setVoxelShape(0, 0, (2F / 16F), 1, (13F / 16F), (14F / 16F));
    }

}
