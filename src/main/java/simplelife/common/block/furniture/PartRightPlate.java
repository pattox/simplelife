package simplelife.common.block.furniture;

public class PartRightPlate extends BasicDirectionalFurniture {

    public static String identifier = "part_right_plate";

    public PartRightPlate(Settings settings) {
        super(settings);
        this.setVoxelShape((15F/16F), 0, 0, 1F, 1F, 1F);
    }

}
