package simplelife.common.block.furniture;

public class PartTableTop extends BasicDirectionalFurniture {

    public static String identifier = "part_table_top";

    public PartTableTop(Settings settings) {
        super(settings);
        this.setVoxelShape(0, (15F/16F), 0, 1F, 1F, 1F);
    }

}
