package simplelife.common.block.furniture;

public class BasicChair extends BasicDirectionalFurniture {

    public static String identifier = "basic_chair";

    public BasicChair(Settings settings) {
        super(settings);
        this.setVoxelShape((2F / 16F), 0, (2F / 16F), (14F / 16F), 1, (15F / 16F));
    }

}
