package simplelife.common.block.furniture;

public class PartLeftPlate extends BasicDirectionalFurniture {

    public static String identifier = "part_left_plate";

    public PartLeftPlate(Settings settings) {
        super(settings);
        this.setVoxelShape(0, 0, 0, (1F/16F), 1F, 1F);
    }


}
