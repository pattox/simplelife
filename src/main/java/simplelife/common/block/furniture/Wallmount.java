package simplelife.common.block.furniture;

public class Wallmount extends BasicDirectionalFurniture {

    public static String identifier = "wallmount";

    public Wallmount(Settings settings) {
        super(settings);
        this.setVoxelShape((5F / 16F), 0, 0, (11F / 16F), (9F / 16F), (9F / 16F));

    }

}
