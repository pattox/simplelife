package simplelife.common.block.furniture;

public class Keyboard extends BasicDirectionalFurniture {

    public static String identifier = "keyboard";

    public Keyboard(Settings settings) {
        super(settings);
        this.setVoxelShape((1F / 16F), 0, (3F / 16F), (15F / 16F), (1F / 16F), (10F / 16F));
    }
}
