package simplelife.common.block.furniture;

@SuppressWarnings("deprecation")
public class WallTv extends BasicDirectionalFurniture {

    public static String identifier = "wall_tv";

    public WallTv(Settings settings) {
        super(settings);
        this.setVoxelShape(0, 0.25F, 0, 1F, (14F / 16F), (2F / 16F));
    }

}
