package simplelife.common.block.entities;

import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.state.property.Properties;
import net.minecraft.util.Tickable;
import net.minecraft.util.math.Direction;
import simplelife.common.registries.Redstones;

public class RedstoneOREntity extends BlockEntity  {

    public RedstoneOREntity(BlockEntityType<?> type) {
        super(type);
    }

    public RedstoneOREntity() {
        super(Redstones.REDSTONE_OR_ENTITY);
    }

}
