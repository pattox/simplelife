package simplelife.common.block.entities;

import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityType;
import simplelife.common.registries.Redstones;

public class RedstoneLockEntity extends BlockEntity {

    public RedstoneLockEntity(BlockEntityType<?> type) {
        super(type);
    }

    public RedstoneLockEntity() {
        super(Redstones.REDSTONE_LOCK_ENTITY);
    }

}
