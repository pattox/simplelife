package simplelife.common.block.entities;

import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.state.property.Properties;
import net.minecraft.util.Tickable;
import net.minecraft.util.math.Direction;
import net.minecraft.world.World;
import simplelife.common.registries.Redstones;

public class RedstoneInverterEntity extends BlockEntity implements Tickable  {

    public RedstoneInverterEntity(BlockEntityType<?> type) {
        super(type);
    }

    public RedstoneInverterEntity() {
        super(Redstones.REDSTONE_INVERTER_ENTITY);
    }

    @Override
    public void tick() {
        World w = getWorld();
        BlockState state = getCachedState();

        Direction eastDirection = state.get(Properties.HORIZONTAL_FACING).rotateYClockwise();
        Direction westDirection = state.get(Properties.HORIZONTAL_FACING).rotateYCounterclockwise();
        boolean east = w.isEmittingRedstonePower(pos.offset(eastDirection, 1), eastDirection);
        boolean west = w.isEmittingRedstonePower(pos.offset(westDirection, 1), westDirection);

        if (east && west) {
            state = state.with(Properties.POWERED, true);
            state = state.with(Properties.POWER, 15);
        } else {
            state = state.with(Properties.POWER, 0);
            state = state.with(Properties.POWERED, false);
        }
        w.setBlockState(getPos(), state);
    }
}
