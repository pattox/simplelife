package simplelife.common.block.entities;

import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.state.property.Properties;
import net.minecraft.util.Tickable;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import simplelife.common.registries.Redstones;

public class RedstoneTimerEntity extends BlockEntity implements Tickable {

    public RedstoneTimerEntity(BlockEntityType<?> type) {
        super(type);
    }

    public RedstoneTimerEntity() {
        super(Redstones.REDSTONE_TIMER_ENTITY);
    }

    private long tickLimit;

    @Override
    public void tick() {
        if (this.world == null || this.world.isClient) {
            return;
        }

        if (world.isReceivingRedstonePower(getPos())) {
            if (this.tickLimit == 20) {
                // We Have a second
                this.toggleRedstoneOutput(getWorld(), getPos(), getCachedState());
                this.tickLimit = 0;
            } else {
                this.tickLimit = this.tickLimit + 1;
            }
        }
    }

    // Toggle output to direction (the backside)
    private void toggleRedstoneOutput(World world, BlockPos pos, BlockState state) {
        int currentState = state.get(Properties.POWER);
        if (currentState == 0) {
            world.setBlockState(pos, state.with(Properties.POWERED, true));
            world.setBlockState(pos, state.with(Properties.POWER, 15));
        } else {
            world.setBlockState(pos, state.with(Properties.POWERED, false));
            world.setBlockState(pos, state.with(Properties.POWER, 0));
        }
    }
}
