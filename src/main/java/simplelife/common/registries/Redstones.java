package simplelife.common.registries;

import net.fabricmc.fabric.api.item.v1.FabricItemSettings;
import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.minecraft.block.Block;
import net.minecraft.block.Material;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import simplelife.common.SimpleLife;
import simplelife.common.block.entities.*;
import simplelife.common.block.redstone.*;
import simplelife.common.item.RedstoneLockKey;

public class Redstones {

    // Identifiers
    public static final Identifier REDSTONE_LOCK_IDENTIFIER = new Identifier(SimpleLife.MOD_ID, "redstone_lock");
    public static final Identifier REDSTONE_TIMER_IDENTIFIER = new Identifier(SimpleLife.MOD_ID, "redstone_timer");

    // Redstone Lock
    public static final Block REDSTONE_LOCK = registerBlock(REDSTONE_LOCK_IDENTIFIER, new RedstoneLock(FabricBlockSettings.of(Material.GLASS).hardness(10f)));
    public static BlockEntityType<RedstoneLockEntity> REDSTONE_LOCK_ENTITY = Registry.register(Registry.BLOCK_ENTITY_TYPE, REDSTONE_LOCK_IDENTIFIER, BlockEntityType.Builder.create(RedstoneLockEntity::new, REDSTONE_LOCK).build(null));
    public static final RedstoneLockKey REDSTONE_LOCK_KEY = registerItem(RedstoneLockKey.identifier, new RedstoneLockKey(new FabricItemSettings().group(SimpleLife.ITEM_GROUP).maxCount(1)));

    // Redstone Timer
    public static final Block REDSTONE_TIMER = registerBlock(REDSTONE_TIMER_IDENTIFIER, new RedstoneTimer(FabricBlockSettings.of(Material.GLASS).hardness(10f)));
    public static BlockEntityType<RedstoneTimerEntity> REDSTONE_TIMER_ENTITY = Registry.register(Registry.BLOCK_ENTITY_TYPE, REDSTONE_TIMER_IDENTIFIER, BlockEntityType.Builder.create(RedstoneTimerEntity::new, REDSTONE_TIMER).build(null));

    // Redstone AND gate
    public static final Block REDSTONE_AND = registerBlock(RedstoneAND.identifier, new RedstoneAND(FabricBlockSettings.of(Material.GLASS).hardness(10f)));
    public static BlockEntityType<RedstoneANDEntity> REDSTONE_AND_ENTITY = Registry.register(Registry.BLOCK_ENTITY_TYPE, RedstoneAND.identifier, BlockEntityType.Builder.create(RedstoneANDEntity::new, REDSTONE_AND).build(null));

    // Redstone NAND gate
    public static final Block REDSTONE_NAND = registerBlock(RedstoneNAND.identifier, new RedstoneNAND(FabricBlockSettings.of(Material.GLASS).hardness(10f)));
    public static BlockEntityType<RedstoneNANDEntity> REDSTONE_NAND_ENTITY = Registry.register(Registry.BLOCK_ENTITY_TYPE, RedstoneNAND.identifier, BlockEntityType.Builder.create(RedstoneNANDEntity::new, REDSTONE_NAND).build(null));

    // Redstone OR gate
    public static final Block REDSTONE_OR = registerBlock(RedstoneOR.identifier, new RedstoneOR(FabricBlockSettings.of(Material.GLASS).hardness(10f)));
    public static BlockEntityType<RedstoneOREntity> REDSTONE_OR_ENTITY = Registry.register(Registry.BLOCK_ENTITY_TYPE, RedstoneOR.identifier, BlockEntityType.Builder.create(RedstoneOREntity::new, REDSTONE_OR).build(null));

    // Redstone Inverter
    public static final Block REDSTONE_INVERTER = registerBlock(RedstoneInverter.identifier, new RedstoneInverter(FabricBlockSettings.of(Material.GLASS).hardness(10f)));
    public static BlockEntityType<RedstoneInverterEntity> REDSTONE_INVERTER_ENTITY = Registry.register(Registry.BLOCK_ENTITY_TYPE, RedstoneInverter.identifier, BlockEntityType.Builder.create(RedstoneInverterEntity::new, REDSTONE_INVERTER).build(null));

    public static void init() {
        // Hi! Hello! Welcome!
    }

    private static Block registerBlock(Identifier identifier, Block block) {
        block = Registry.register(Registry.BLOCK, identifier, block);
        Registry.register(Registry.ITEM, identifier, new BlockItem(block, new FabricItemSettings().group(SimpleLife.ITEM_GROUP)));
        return block;
    }

    private static <I extends Item> I registerItem(String identifier, I item) {
        Registry.register(Registry.ITEM, new Identifier(SimpleLife.MOD_ID, identifier), item);
        return item;
    }
}
