package simplelife.common.registries;

import net.fabricmc.fabric.api.item.v1.FabricItemSettings;
import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.minecraft.block.Block;
import net.minecraft.block.Material;
import net.minecraft.item.BlockItem;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import simplelife.common.SimpleLife;
import simplelife.common.block.outdoor.HedgeOakFull;

public class Outdoor {

    // Hedges
    //public static final Block HEDGE_OAK_FULL = registerBlock(HedgeOakFull.identifier, new HedgeOakFull(FabricBlockSettings.of(Material.LEAVES).nonOpaque()));


    public static void init() {
        // This space is available for rent
    }

    private static Block registerBlock(String identifier, Block block) {
        Registry.register(Registry.ITEM, new Identifier(SimpleLife.MOD_ID, identifier),
                new BlockItem(block, new FabricItemSettings().group(SimpleLife.ITEM_GROUP))
        );
        return Registry.register(Registry.BLOCK, new Identifier(SimpleLife.MOD_ID, identifier), block);

    }
}
