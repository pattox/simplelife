package simplelife.common.util;

import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.world.World;

public class RedstoneLogic {

    public static boolean hasLeftInput(World world, BlockPos pos) {
        return world.isEmittingRedstonePower(pos.west(), Direction.EAST);
    }

}
